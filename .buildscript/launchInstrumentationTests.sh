#!/bin/sh
instrumentationResult=$(adb shell 'am instrument -w me.sergio.groopifytest.mock.test/me.sergio.groopifytest.groopifyTestRunner ; printf "$?"')
printf "$instrumentationResult\n"
exitCode=$(printf "$instrumentationResult" | tail -1)
if [ $exitCode != "0" ]; then
  exit 1
fi