package me.sergio.groopifytest.data;

import java.util.ArrayList;
import java.util.List;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.model.ArtistLocalGateway;

public class InMemoryArtistGateway implements ArtistLocalGateway {

  private final List<Artist> artists = new ArrayList<>();

  @Override public void persist(List<Artist> artists) {
    for (Artist artist: artists) {
      persistAlbums(artist);
    }
    this.artists.addAll(artists);
  }


  private void persistAlbums(Artist artist) {
    for (Artist a: artists) {
      if(a.equals(artist)){
        a.setAlbums(artist.getAlbums());
      }
    }
  }

  @Override public Artist obtain(String name) {
    for (Artist c : artists) {
      if (c.getName().equals(name)) return c;
    }
    return null;
  }

  @Override public void delete(List<Artist> deleted) {
    artists.removeAll(deleted);
  }

  @Override public List<Artist> obtainArtists() {
    return artists;
  }
}
