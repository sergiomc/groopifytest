package me.sergio.groopifytest.data;

import android.content.Context;

import com.google.gson.reflect.TypeToken;

import me.sergio.groopifytest.data.repository.artists.datasources.api.ArtistApiService;
import me.sergio.groopifytest.data.repository.artists.datasources.api.responses.ApiResponse;
import retrofit.Call;

public class MockArtistsApiService implements ArtistApiService {

  private final Context applicationContext;

  public MockArtistsApiService(Context applicationContext) {
    this.applicationContext = applicationContext.getApplicationContext();
  }

  @Override public Call<ApiResponse> obtainArtist(int id, String entity, String media, int limit) {
    return new LocalFileCall<>(new TypeToken<ApiResponse>() {
    }.getType(), applicationContext.getAssets(), "artist.json");
  }
}
