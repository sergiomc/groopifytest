package me.sergio.groopifytest.data;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.data.repository.artists.datasources.api.ArtistApiService;

@Module(
    complete = false,
    library = true,
    overrides = true) public class ApiModuleMock {

  @Provides @Singleton
  ArtistApiService provideApiService(Application context) {
    return new MockArtistsApiService(context);
  }

}
