package me.sergio.groopifytest.data;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.domain.model.ArtistLocalGateway;

@Module(complete = false,
    library = true,
    overrides = true) public class BddModuleMock {

  @Provides @Singleton
  ArtistLocalGateway provideArtistDao() {
    return new InMemoryArtistGateway();
  }
}
