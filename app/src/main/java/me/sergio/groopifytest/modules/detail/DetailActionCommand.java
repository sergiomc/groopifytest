package me.sergio.groopifytest.modules.detail;

import android.app.Activity;
import android.content.Intent;

import me.sergio.groopifytest.ui.ActionCommand;

public class DetailActionCommand implements ActionCommand {

  private Activity activity;
  private String artistName;

  public DetailActionCommand(Activity activity, String artistName) {
    this.activity = activity;
    this.artistName = artistName;
  }

  @Override public void execute() {
    Intent intent = new Intent(activity, DetailActivity.class);
    intent.putExtra(DetailActivity.ARTIST_NAME_EXTRA, artistName);
    activity.startActivity(intent);
  }
}
