package me.sergio.groopifytest.modules.main;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyRecyclerAdapter;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import me.sergio.groopifytest.R;
import me.sergio.groopifytest.modules.detail.DetailActionCommand;
import me.sergio.groopifytest.modules.main.adapters.ArtistViewHolderFactory;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;
import me.sergio.groopifytest.presentation.modules.main.MainPresenter;
import me.sergio.groopifytest.presentation.modules.main.MainView;
import me.sergio.groopifytest.ui.activity.BaseActivity;
import me.sergio.groopifytest.ui.elevation.ElevationHandler;
import me.sergio.groopifytest.ui.errors.ErrorManager;
import me.sergio.groopifytest.ui.imageloader.ImageLoader;
import me.sergio.groopifytest.ui.items.AlbumViewHolder;

public class MainActivity extends BaseActivity
    implements MainView, SwipeRefreshLayout.OnRefreshListener, EasyViewHolder.OnItemClickListener {

  @Inject MainPresenter presenter;
  @Inject ErrorManager errorManager;
  @Inject ImageLoader imageLoader;
  @Inject ElevationHandler elevationHandler;

  @Bind(R.id.swipeRefreshLayout) SwipeRefreshLayout swipeRefreshLayout;
  @Bind(R.id.recyclerView) RecyclerView recyclerView;
  @Bind(R.id.empty_list) TextView emptyList;
  @Bind(R.id.toolbar) Toolbar toolbar;

  private EasyRecyclerViewManager recyclerViewManager;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter.attachView(this);
  }

  @Override public void initUi() {
    initToolbar();
    initRecyclerView();
    initRefreshLayout();
  }

  private void initToolbar() {
    if (toolbar != null) {
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(false);
      elevationHandler.setDefaultElevation(toolbar);
    }
  }

  private void initRecyclerView() {
    ArtistViewHolderFactory artistViewHolderFactory =
        new ArtistViewHolderFactory(this, imageLoader);
    EasyRecyclerAdapter adapter =
        new EasyRecyclerAdapter(artistViewHolderFactory, ArtistPresentationBase.class,
                AlbumViewHolder.class);
    recyclerViewManager =
        new EasyRecyclerViewManager.Builder(recyclerView, adapter).emptyLoadingListTextView(
            emptyList)
            .loadingListTextColor(android.R.color.black)
            .divider(R.drawable.list_divider)
            .clickListener(this)
            .build();
  }

  private void initRefreshLayout() {
    swipeRefreshLayout.setOnRefreshListener(this);
    recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

      public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
      }

      public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        int topRowVerticalPosition = recyclerView == null || recyclerView.getChildCount() == 0 ? 0
            : recyclerView.getChildAt(0).getTop();
        swipeRefreshLayout.setEnabled(topRowVerticalPosition >= 0);
      }
    });
  }

  @Override public int onCreateViewId() {
    return R.layout.activity_main;
  }

  @Override protected void onResume() {
    super.onResume();
    presenter.onResume();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @Override public void refreshArtistsList(final List<ArtistPresentationBase> artists) {
    recyclerViewManager.addAll(artists);
    swipeRefreshLayout.setRefreshing(false);
  }

  @Override public void showGetArtistsError() {
    errorManager.showError(getString(R.string.err_getting_artists));
  }

  @Override public void onRefresh() {
    presenter.onRefresh();
  }

  @Override public void refreshUi() {
    recyclerViewManager.onRefresh();
    swipeRefreshLayout.setRefreshing(true);
  }

  @Override protected MainModule newDiModule() {
    return new MainModule();
  }

  @Override public void onItemClick(int position, View view) {
    ArtistPresentationBase artist = (ArtistPresentationBase) recyclerViewManager.getItem(position);
    DetailActionCommand detailActionCommand =
        new DetailActionCommand(this, artist.getName());
    detailActionCommand.execute();
  }
}
