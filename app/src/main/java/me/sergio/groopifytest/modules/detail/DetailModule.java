package me.sergio.groopifytest.modules.detail;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.di.ActivityModule;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistInteractor;
import me.sergio.groopifytest.presentation.GroopifyTestViewInjector;
import me.sergio.groopifytest.presentation.invoker.InteractorInvoker;
import me.sergio.groopifytest.presentation.model.mapper.PresentationArtistMapper;
import me.sergio.groopifytest.presentation.modules.detail.DetailPresenter;

@Module(
    addsTo = ActivityModule.class,
    library = true,
    injects = DetailActivity.class) public class DetailModule {

  private String artistMd5;

  public DetailModule(String artistMd5) {
    this.artistMd5 = artistMd5;
  }

  @Provides DetailPresenter providePresenter(InteractorInvoker interactorInvoker,
      GetArtistInteractor getArtistInteractor, PresentationArtistMapper artistMapper,
      GroopifyTestViewInjector viewInjector) {
    return new DetailPresenter(artistMd5, interactorInvoker, getArtistInteractor, artistMapper,
        viewInjector);
  }

}
