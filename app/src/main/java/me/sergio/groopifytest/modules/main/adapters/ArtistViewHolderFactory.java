package me.sergio.groopifytest.modules.main.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.carlosdelachica.easyrecycleradapters.adapter.BaseEasyViewHolderFactory;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import me.sergio.groopifytest.ui.imageloader.ImageLoader;
import me.sergio.groopifytest.ui.items.ArtistViewHolder;

/**
 * Created by sergio on 14/1/16.
 */
public class ArtistViewHolderFactory extends BaseEasyViewHolderFactory {

  private final ImageLoader imageLoader;

  public ArtistViewHolderFactory(Context context, ImageLoader imageLoader) {
    super(context);
    this.imageLoader = imageLoader;
  }

  @Override public EasyViewHolder create(Class valueClass, ViewGroup parent) {
    return new ArtistViewHolder(context, parent, imageLoader);
  }

}
