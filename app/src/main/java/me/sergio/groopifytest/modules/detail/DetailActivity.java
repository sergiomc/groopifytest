package me.sergio.groopifytest.modules.detail;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyRecyclerAdapter;
import com.carlosdelachica.easyrecycleradapters.recycler_view_manager.EasyRecyclerViewManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import me.sergio.groopifytest.R;
import me.sergio.groopifytest.modules.detail.adapters.AlbumViewHolderFactory;
import me.sergio.groopifytest.presentation.model.AlbumPresentationBase;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;
import me.sergio.groopifytest.presentation.modules.detail.DetailPresenter;
import me.sergio.groopifytest.presentation.modules.detail.DetailView;
import me.sergio.groopifytest.ui.activity.BaseActivity;
import me.sergio.groopifytest.ui.errors.ErrorManager;
import me.sergio.groopifytest.ui.imageloader.ImageLoader;
import me.sergio.groopifytest.ui.items.AlbumViewHolder;

public class DetailActivity extends BaseActivity
    implements DetailView{

  public static final String ARTIST_NAME_EXTRA = "ArtistExtra";

  @Inject DetailPresenter presenter;
  @Inject ImageLoader imageLoader;
  @Inject ErrorManager errorManager;

  @Bind(R.id.recyclerView) RecyclerView recyclerView;
  @Bind(R.id.toolbar) Toolbar toolbar;

  private ArtistPresentationBase artist;

  private EasyRecyclerViewManager recyclerViewManager;
  private EasyRecyclerAdapter adapter;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    presenter.attachView(this);
  }

  @Override public int onCreateViewId() {
    return R.layout.activity_detail;
  }

  @Override protected void onResume() {
    super.onResume();
    presenter.onResume();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    presenter.detachView();
  }

  @Override public void initUi() {
    initToolbar();
    initRecyclerView();
  }

  private void initToolbar() {
    if (toolbar != null) {
      setSupportActionBar(toolbar);
      getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
  }

  private void initRecyclerView() {
    AlbumViewHolderFactory albumViewHolderFactory =
            new AlbumViewHolderFactory(this, imageLoader);
    EasyRecyclerAdapter adapter =
            new EasyRecyclerAdapter(albumViewHolderFactory, AlbumPresentationBase.class,
                    AlbumViewHolder.class);
    recyclerViewManager =
            new EasyRecyclerViewManager.Builder(recyclerView, adapter)
                    .layoutManager(new GridLayoutManager(getApplicationContext(), getResources().getInteger(R.integer.grid_columns)))
                    .loadingListTextColor(android.R.color.black)
                    .build();
  }

  @Override public void showArtistAlbums(ArtistPresentationBase artist) {
    this.artist = artist;
    if (artist != null) {
      List<AlbumPresentationBase> presentationAlbums = artist.getAlbums();
      recyclerViewManager.addAll(presentationAlbums);
      showArtistName();
    }
  }

  private void showArtistName() {
    toolbar.setTitle(artist.getName());
  }

  @Override public void showGetArtistError() {
    errorManager.showError(getString(R.string.err_getting_artists));
  }

  @Override protected Object newDiModule() {
    return new DetailModule(getIntent().getStringExtra(ARTIST_NAME_EXTRA));
  }
}
