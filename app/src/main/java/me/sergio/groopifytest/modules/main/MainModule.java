package me.sergio.groopifytest.modules.main;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.di.ActivityModule;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistsInteractor;
import me.sergio.groopifytest.domain.mappers.ListMapper;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.presentation.GroopifyTestViewInjector;
import me.sergio.groopifytest.presentation.invoker.InteractorInvoker;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;
import me.sergio.groopifytest.presentation.modules.main.MainPresenter;

@Module(
    addsTo = ActivityModule.class,
    injects = MainActivity.class) public class MainModule {

  @Provides @Singleton MainPresenter provideMainPresenter(InteractorInvoker interactorInvoker,
      GetArtistsInteractor getArtistsInteractor,
      ListMapper<Artist, ArtistPresentationBase> listMapper, GroopifyTestViewInjector viewInjector) {
    return new MainPresenter(interactorInvoker, getArtistsInteractor, listMapper, viewInjector);
  }
}
