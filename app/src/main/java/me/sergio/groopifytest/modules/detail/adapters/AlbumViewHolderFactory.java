package me.sergio.groopifytest.modules.detail.adapters;

import android.content.Context;
import android.view.ViewGroup;

import com.carlosdelachica.easyrecycleradapters.adapter.BaseEasyViewHolderFactory;
import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import me.sergio.groopifytest.ui.imageloader.ImageLoader;
import me.sergio.groopifytest.ui.items.AlbumViewHolder;

/**
 * Created by sergio on 14/1/16.
 */
public class AlbumViewHolderFactory extends BaseEasyViewHolderFactory {

  private final ImageLoader imageLoader;

  public AlbumViewHolderFactory(Context context, ImageLoader imageLoader) {
    super(context);
    this.imageLoader = imageLoader;
  }

  @Override public EasyViewHolder create(Class valueClass, ViewGroup parent) {

    return new AlbumViewHolder(context, parent, imageLoader);
  }

}
