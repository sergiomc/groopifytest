package me.sergio.groopifytest;

import android.app.Application;
import android.content.Context;
import dagger.ObjectGraph;
import me.sergio.groopifytest.di.AppModule;

public class GroopifyTestApp extends Application {

  private ObjectGraph objectGraph;

  @Override public void onCreate() {
    super.onCreate();
    initObjectGraph();
  }

  private void initObjectGraph() {
    objectGraph = ObjectGraph.create(new AppModule(this));
    inject(this);
  }

  public void inject(Object object) {
    objectGraph.inject(object);
  }

  public static GroopifyTestApp get(Context context) {
    return (GroopifyTestApp) context.getApplicationContext();
  }

  public ObjectGraph getObjectGraph() {
    return objectGraph;
  }

  public void setObjectGraph(ObjectGraph objectGraph) {
    this.objectGraph = objectGraph;
  }
}
