package me.sergio.groopifytest.di;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.ui.elevation.ElevationHandler;
import me.sergio.groopifytest.ui.elevation.ElevationHandlerFactory;
import me.sergio.groopifytest.ui.errors.ErrorManager;
import me.sergio.groopifytest.ui.errors.SnackbarErrorManagerImp;
import me.sergio.groopifytest.ui.transitions.WindowTransitionListener;
import me.sergio.groopifytest.ui.transitions.WindowTransitionListenerFactory;

@Module(addsTo = AppModule.class, library = true) public class ActivityModule {

  private AppCompatActivity activity;

  public ActivityModule(AppCompatActivity activity) {
    this.activity = activity;
  }

  @Provides ActionBar provideActionBar() {
    return activity.getSupportActionBar();
  }

  @Provides Context provideContext() {
    return activity;
  }

  @Provides AppCompatActivity provideActivity() {
    return activity;
  }

  @Provides ErrorManager provideErrorManager() {
    return new SnackbarErrorManagerImp(activity);
  }

  @Provides Window provideWindow() {
    return activity.getWindow();
  }

  @Provides @Singleton ElevationHandler.Factory provideElevationHandlerFactory(
      ElevationHandlerFactory factoryImp) {
    return factoryImp;
  }

  @Provides @Singleton ElevationHandler provideElevationHandler(ElevationHandler.Factory factory) {
    return factory.createElevationHandler();
  }

  @Provides @Singleton WindowTransitionListener.Factory provideWindowTransitionListenerFactory(
      WindowTransitionListenerFactory factoryImp) {
    return factoryImp;
  }

  @Provides @Singleton WindowTransitionListener provideElevationHandler(WindowTransitionListener.Factory factory) {
    return factory.createWindowTransitionListener();
  }
  
}
