package me.sergio.groopifytest.di;

import android.app.Application;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.BuildConfig;
import me.sergio.groopifytest.data.DatabaseHelper;
import me.sergio.groopifytest.data.DatabaseName;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.ArtistLocalGatewayImp;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddAlbum;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddArtist;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.mapper.BddAlbumMapper;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.mapper.BddArtistMapper;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors.AlbumPersistor;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors.ArtistPersistor;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors.Persistor;
import me.sergio.groopifytest.data.repository.caching.strategy.CachingStrategy;
import me.sergio.groopifytest.data.repository.caching.strategy.list.ListCachingStrategy;
import me.sergio.groopifytest.data.repository.caching.strategy.nullsafe.NotNullCachingStrategy;
import me.sergio.groopifytest.data.repository.caching.strategy.ttl.TtlCachingStrategy;
import me.sergio.groopifytest.domain.mappers.TwoWaysMapper;
import me.sergio.groopifytest.domain.model.Album;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.model.ArtistLocalGateway;

@Module(
    complete = false,
    library = true) public class BddModule {

  @Provides @Singleton CachingStrategy<BddArtist> provideArtistCachingStrategy() {
    return new NotNullCachingStrategy<>();
  }

  @Provides @Singleton ListCachingStrategy<BddArtist> provideListArtistCachingStrategy() {
    return new ListCachingStrategy<>(new TtlCachingStrategy<BddArtist>(3, TimeUnit.MINUTES));
  }

  @Provides @Singleton
  ArtistLocalGateway provideArtistsLocalGateway(
          Persistor<BddArtist> artistPersistor, Persistor<BddAlbum> albumPersistor, DatabaseHelper helper,
          CachingStrategy<BddArtist> singleArtistCachingStrategy,
          ListCachingStrategy<BddArtist> listCachingStrategy, TwoWaysMapper<BddArtist, Artist> artistMapper, TwoWaysMapper<BddAlbum, Album> albumMapper) {
    return new ArtistLocalGatewayImp(artistPersistor, albumPersistor, helper.getArtistDao(), helper.getAlbumDao(),
        singleArtistCachingStrategy, listCachingStrategy, artistMapper, albumMapper);
  }

  @Provides @Singleton TwoWaysMapper<BddArtist, Artist> provideBddArtistMapper() {
    return new BddArtistMapper();
  }

  @Provides @Singleton TwoWaysMapper<BddAlbum, Album> provideBddAlbumMapper() {
    return new BddAlbumMapper();
  }

  @Provides @Singleton Persistor<BddArtist> provideArtistPersistor(DatabaseHelper helper) {
    return new ArtistPersistor(helper);
  }

  @Provides @Singleton Persistor<BddAlbum> provideAlbumPersistor(DatabaseHelper helper) {
    return new AlbumPersistor(helper);
  }

  @Provides @Singleton
  public DatabaseHelper provideDatabaseHelper(@DatabaseName String databaseName, Application app) {
    return new DatabaseHelper(databaseName, app);
  }

  @Provides @Singleton @DatabaseName String provideDatabaseName() {
    return "groopifyTest" + (BuildConfig.DEBUG ? "-dev" : "") + ".db";
  }
}
