package me.sergio.groopifytest.di;

import android.app.Application;
import com.squareup.picasso.Picasso;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import me.sergio.groopifytest.di.qualifiers.UiThread;
import me.sergio.groopifytest.presentation.GroopifyTestViewInjector;
import me.sergio.groopifytest.presentation.ViewInjectorImp;
import me.sergio.groopifytest.ui.imageloader.ImageLoader;
import me.sergio.groopifytest.ui.imageloader.PicassoImageLoader;
import me.panavtec.threaddecoratedview.views.ThreadSpec;

@Module(
    complete = false,
    library = true) public class UiModule {

  @Provides @Singleton Picasso providePicasso(Application app) {
    return Picasso.with(app);
  }

  @Provides @Singleton ImageLoader provideImageLoader(Picasso picasso) {
    return new PicassoImageLoader(picasso);
  }

  @Provides @Singleton
  GroopifyTestViewInjector provideViewInjector(ViewInjectorImp imp) {
    return imp;
  }

  @Provides @Singleton ViewInjectorImp provieViewInjectorImp(@UiThread ThreadSpec threadSpec) {
    return new ViewInjectorImp(threadSpec);
  }
}
