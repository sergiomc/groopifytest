package me.sergio.groopifytest.di;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.List;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.data.Endpoint;
import me.sergio.groopifytest.data.HeadersInterceptor;
import me.sergio.groopifytest.data.RetrofitLog;
import me.sergio.groopifytest.data.UserAgent;
import me.sergio.groopifytest.data.repository.artists.datasources.api.ArtistNetworkGatewayImp;
import me.sergio.groopifytest.data.repository.artists.datasources.api.ArtistApiService;
import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.ApiResult;
import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.mapper.ApiArtistMapper;
import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.mapper.ApiAlbumMapper;
import me.sergio.groopifytest.data.repository.artists.datasources.api.interceptors.HeadersInterceptorImpl;
import me.sergio.groopifytest.domain.mappers.Mapper;
import me.sergio.groopifytest.domain.model.Album;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.model.ArtistNetworkGateway;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

@Module(
    complete = false,
    library = true) public class ApiModule {

  @Provides @Singleton
  ArtistNetworkGateway provideArtistsNetworkDataSource(
          ArtistApiService apiService, Mapper<ApiResult, Artist> artistMapper, Mapper<ApiResult, Album> albumMapper) {
    return new ArtistNetworkGatewayImp(apiService, artistMapper, albumMapper);
  }

  @Provides @Singleton Mapper<ApiResult, Artist> provideMapper() {
    return new ApiArtistMapper();
  }

  @Provides @Singleton Mapper<ApiResult, Album> provideAlbumMapper() {
    return new ApiAlbumMapper();
  }

  @Provides @Singleton
  ArtistApiService provideApiService(@Endpoint String enpoint,
      GsonConverterFactory gsonConverterFactory, OkHttpClient okClient) {

    Retrofit retrofit = new Retrofit.Builder().baseUrl(enpoint)
        .client(okClient)
        .addConverterFactory(gsonConverterFactory)
        .build();

    return retrofit.create(ArtistApiService.class);
  }

  @Provides @Singleton HttpLoggingInterceptor provideLoggingInterceptor() {
    HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
    interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
    return interceptor;
  }

  @Provides @Singleton @HeadersInterceptor Interceptor provideHeadersInterceptor(
      @UserAgent String userAgent) {
    return new HeadersInterceptorImpl(userAgent);
  }

  @Provides @Singleton OkHttpClient provideOkClient(@RetrofitLog boolean retrofitLog,
      @HeadersInterceptor Interceptor headersInterceptor,
      HttpLoggingInterceptor loggingInterceptor) {
    OkHttpClient okHttpClient = new OkHttpClient();
    List<Interceptor> interceptors = okHttpClient.interceptors();
    interceptors.add(headersInterceptor);

    if (retrofitLog) {
      interceptors.add(loggingInterceptor);
    }

    return okHttpClient;
  }

  @Provides @Singleton GsonConverterFactory provideGsonConverterFactory() {
    return GsonConverterFactory.create();
  }
}
