package me.sergio.groopifytest.di;

import android.os.Build;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.BuildConfig;
import me.sergio.groopifytest.data.Endpoint;
import me.sergio.groopifytest.data.RetrofitLog;
import me.sergio.groopifytest.data.UserAgent;

@Module(
    includes = {
        InteractorsModule.class, BddModule.class, ApiModule.class
    },
    complete = false,
    library = true) public class DataModule {

  @Provides @Singleton @Endpoint String provideEndpoint() {
    return BuildConfig.API_URL;
  }

  @Provides @Singleton @RetrofitLog boolean provideRetrofitLog() {
    return BuildConfig.RETROFIT_LOG;
  }

  @Provides @Singleton @UserAgent String provideUserAgent() {
    return String.format("Sample-Android;%s;%s;%s;%d;", Build.MANUFACTURER, Build.MODEL,
        Build.VERSION.RELEASE, BuildConfig.VERSION_CODE);
  }
}
