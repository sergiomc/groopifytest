package me.sergio.groopifytest.di;

import android.app.Application;
import android.os.Build;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.GroopifyTestApp;
import me.sergio.groopifytest.di.qualifiers.ApiLevel;

@Module(
    includes = {
        DataModule.class, UiModule.class, DomainModule.class, PresentationModule.class
    },
    injects = GroopifyTestApp.class,
    library = true
)
public class AppModule {

  private final Application app;

  public AppModule(Application app) {
    this.app = app;
  }

  @Provides @Singleton Application provideApplication() {
    return app;
  }

  @Provides @Singleton @ApiLevel
  int provideApiLevel() {
    return Build.VERSION.SDK_INT;
  }
}
