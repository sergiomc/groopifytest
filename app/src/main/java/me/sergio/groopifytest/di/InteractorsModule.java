package me.sergio.groopifytest.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistInteractor;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistsInteractor;
import me.sergio.groopifytest.domain.model.ArtistLocalGateway;
import me.sergio.groopifytest.domain.model.ArtistNetworkGateway;

@Module(
    complete = false,
    library = true) public class InteractorsModule {

  @Provides @Singleton
  GetArtistsInteractor provideGetArtistsInteractor(
          ArtistLocalGateway localGateway, ArtistNetworkGateway networkGateway) {
    return new GetArtistsInteractor(localGateway, networkGateway);
  }

  @Provides @Singleton
  GetArtistInteractor provideGetArtistInteractor(
          ArtistLocalGateway localGateway) {
    return new GetArtistInteractor(localGateway);
  }
}
