package me.sergio.groopifytest.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.panavtec.threaddecoratedview.views.ThreadSpec;
import me.sergio.groopifytest.di.qualifiers.BackThread;
import me.sergio.groopifytest.di.qualifiers.SameThread;
import me.sergio.groopifytest.di.qualifiers.UiThread;
import me.sergio.groopifytest.domain.mappers.ListMapper;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.outputs.BackThreadSpec;
import me.sergio.groopifytest.domain.outputs.MainThreadSpec;
import me.sergio.groopifytest.domain.outputs.SameThreadSpec;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;
import me.sergio.groopifytest.presentation.model.mapper.PresentationArtistMapper;

@Module(
    complete = false,
    library = true
)
public class PresentationModule {

  @Provides @Singleton @UiThread
  ThreadSpec provideMainThread() {
    return new MainThreadSpec();
  }

  @Provides @Singleton @SameThread
  ThreadSpec provideSameThread() {
    return new SameThreadSpec();
  }

  @Provides @Singleton @BackThread
  ThreadSpec provideBackThread() {
    return new BackThreadSpec();
  }

  @Provides @Singleton PresentationArtistMapper providePresentationArtistMapper() {
    return new PresentationArtistMapper();
  }

  @Provides @Singleton ListMapper<Artist, ArtistPresentationBase> providePresentationArtistMapper(
          PresentationArtistMapper presentationArtistMapper) {
    return new ListMapper<>(presentationArtistMapper);
  }
  
}
