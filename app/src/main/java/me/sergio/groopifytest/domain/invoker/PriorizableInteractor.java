package me.sergio.groopifytest.domain.invoker;

public interface PriorizableInteractor {
  int getPriority();
  String getDescription();
}
