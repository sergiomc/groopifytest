package me.sergio.groopifytest.ui.elevation;

import android.view.View;

public interface ElevationHandler {
  void setElevation(View view, float elevation);
  void setDefaultElevation(View view);
  
  interface Factory {
    ElevationHandler createElevationHandler();
  }
}
