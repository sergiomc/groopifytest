package me.sergio.groopifytest.ui.items;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.carlosdelachica.easyrecycleradapters.adapter.EasyViewHolder;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.sergio.groopifytest.R;
import me.sergio.groopifytest.presentation.model.PresentationBase;
import me.sergio.groopifytest.ui.imageloader.ImageLoader;

public class ArtistViewHolder extends EasyViewHolder<PresentationBase> {

  @Bind(R.id.imageView) ImageView imageView;
  @Bind(R.id.nameTextView) TextView nameTextView;

  private ImageLoader imageLoader;
  private Drawable placeholder;

  public ArtistViewHolder(Context context, ViewGroup parent, ImageLoader imageLoader) {
    super(context, parent, R.layout.item_artist);
    this.imageLoader = imageLoader;
    ButterKnife.bind(this, itemView);
    initPlaceHolder(context);
  }

  private void initPlaceHolder(Context context) {
    Drawable placeHolderResourceDrawable =
        ContextCompat.getDrawable(context, R.drawable.ic_action_headphones);
    int accentColor = ContextCompat.getColor(context, R.color.accent);
    placeholder = placeHolderResourceDrawable.mutate();
    placeholder.setColorFilter(accentColor, PorterDuff.Mode.SRC_IN);
  }

  @Override public void bindTo(PresentationBase album) {
    nameTextView.setText(album.getName());
    imageLoader.load(album.getPicture(), imageView, placeholder);
  }
}