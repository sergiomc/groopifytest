package me.sergio.groopifytest.ui.errors;

public interface ErrorManager {
  void showError(String error);
}