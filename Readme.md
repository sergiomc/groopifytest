#Prueba técnica Groopify - Android Developer
Queremos desarrollar una aplicación en la que se muestre una vista con una lista de 3 artistas y, al pulsar en uno de ellos, se pueda consultar otra vista pero en este caso mostrando los álbumes de dicho artista.
Para cada artista hay que mostrar una imagen de su álbum más reciente y el nombre del artista. Para cada álbum, el nombre del álbum y la imagen de la carátula.

Se deben usar estas 3 URLs que contienen toda la información necesaria para cada uno de los artistas a mostrar:

https://itunes.apple.com/lookup?id=909253&entity=album&media=music&limit=10 

https://itunes.apple.com/lookup?id=6906197&entity=album&media=music&limit=20 

https://itunes.apple.com/lookup?id=16252655&entity=album&media=music&limit=7

La información necesaria se encuentra en los parámetros con clave “collectionName”, “artistName” y “artworkUrl100”. El primer resultado del array contiene la información del artista, el resto, a los álbumes.

###Notas:


● Se debe crear un proyecto para Android que posteriormente se pueda compilar y
ejecutar en un simulador o dispositivo. Deberás enviarnos el código fuente (o ponerlo en algún repositorio al que tengamos acceso) del proyecto y un fichero .apk con la aplicación una vez que termines.

● La interfaz debe adaptarse a pantallas de distintos tamaños y resoluciones.

● El diseño de la interfaz de la aplicación es totalmente libre, el único requisito es que se
muestre los datos solicitados para cada vista.

● Se puede utilizar algún framework/librería que facilite tareas como el acceso a los datos
externos, por ejemplo. (¡No reinventes la rueda!)

● Además de evaluarse a nivel técnico el código (estructura del proyecto, limpieza, etc...),
en este ejercicio se valorará igualmente a nivel UI / UX (salvando las distancias con una aplicación para publicar en Google Play). Es decir, se tendrá en cuenta y se valorará muy positivamente que la aplicación se comporte correctamente en cualquier circunstancia o caso de uso habitual que le pueda dar un usuario.

● Si hay algo que quieras comentar sobre las decisiones que has tomado o el porqué de algo que has hecho, por favor escribenos un par de líneas :)