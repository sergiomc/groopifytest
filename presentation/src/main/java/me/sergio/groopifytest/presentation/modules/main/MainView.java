package me.sergio.groopifytest.presentation.modules.main;

import java.util.List;

import me.panavtec.threaddecoratedview.views.qualifiers.NotDecorated;
import me.panavtec.threaddecoratedview.views.qualifiers.ThreadDecoratedView;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;

@ThreadDecoratedView public interface MainView {

  @NotDecorated void initUi();

  void showGetArtistsError();

  void refreshArtistsList(List<ArtistPresentationBase> artists);

  void refreshUi();
}
