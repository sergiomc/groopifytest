package me.sergio.groopifytest.presentation.model;

import java.util.ArrayList;
import java.util.List;

public class ArtistPresentationBase extends PresentationBase {

  public List<AlbumPresentationBase> albums = new ArrayList<>();

  public List<AlbumPresentationBase> getAlbums() {
    return albums;
  }

  public void setAlbums(List<AlbumPresentationBase> albums) {
    this.albums = albums;
  }


}
