package me.sergio.groopifytest.presentation.modules.detail;

import me.panavtec.threaddecoratedview.views.qualifiers.NotDecorated;
import me.panavtec.threaddecoratedview.views.qualifiers.ThreadDecoratedView;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;


@ThreadDecoratedView
public interface DetailView {

  @NotDecorated void initUi();

  void showArtistAlbums(ArtistPresentationBase artist);

  void showGetArtistError();
}
