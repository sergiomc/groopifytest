package me.sergio.groopifytest.presentation;

public interface GroopifyTestViewInjector {
  <V> V injectView(V view);
  <V> V nullObjectPatternView(V view);
}
