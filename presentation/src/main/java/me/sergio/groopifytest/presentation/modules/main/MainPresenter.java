package me.sergio.groopifytest.presentation.modules.main;

import java.util.List;

import me.sergio.groopifytest.domain.interactors.artists.GetArtistsInteractor;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistsError;
import me.sergio.groopifytest.domain.mappers.ListMapper;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.presentation.GroopifyTestViewInjector;
import me.sergio.groopifytest.presentation.InteractorResult;
import me.sergio.groopifytest.presentation.Presenter;
import me.sergio.groopifytest.presentation.invoker.InteractorExecution;
import me.sergio.groopifytest.presentation.invoker.InteractorInvoker;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;

public class MainPresenter extends Presenter<MainView> {

  private final InteractorInvoker interactorInvoker;
  private final GetArtistsInteractor getArtistsInteractor;
  private final ListMapper<Artist, ArtistPresentationBase> listMapper;

  public MainPresenter(InteractorInvoker interactorInvoker,
      GetArtistsInteractor getArtistsInteractor,
      ListMapper<Artist, ArtistPresentationBase> listMapper, GroopifyTestViewInjector viewInjector) {
    super(viewInjector);
    this.interactorInvoker = interactorInvoker;
    this.getArtistsInteractor = getArtistsInteractor;
    this.listMapper = listMapper;
  }

  @Override public void onViewAttached() {
    getView().initUi();
  }

  public void onResume() {
    refreshArtistsList();
  }

  public void onRefresh() {
    getView().refreshUi();
    refreshArtistsList();
  }

  private void refreshArtistsList() {
    new InteractorExecution<>(getArtistsInteractor).result(new InteractorResult<List<Artist>>() {
      @Override public void onResult(List<Artist> result) {
        List<ArtistPresentationBase> presentationArtists = listMapper.map(result);
        getView().refreshArtistsList(presentationArtists);
      }
    }).error(GetArtistsError.class, new InteractorResult<GetArtistsError>() {
      @Override public void onResult(GetArtistsError error) {
        getView().showGetArtistsError();
      }
    }).execute(interactorInvoker);
  }
}
