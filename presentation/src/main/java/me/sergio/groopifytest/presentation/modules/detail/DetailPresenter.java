package me.sergio.groopifytest.presentation.modules.detail;

import me.sergio.groopifytest.domain.interactors.InteractorError;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistInteractor;
import me.sergio.groopifytest.domain.interactors.artists.GetArtistsError;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.presentation.GroopifyTestViewInjector;
import me.sergio.groopifytest.presentation.InteractorResult;
import me.sergio.groopifytest.presentation.Presenter;
import me.sergio.groopifytest.presentation.invoker.InteractorExecution;
import me.sergio.groopifytest.presentation.invoker.InteractorInvoker;
import me.sergio.groopifytest.presentation.model.mapper.PresentationArtistMapper;

public class DetailPresenter extends Presenter<DetailView> {

  private final String artistName;
  private final InteractorInvoker interactorInvoker;
  private final GetArtistInteractor getArtistInteractor;
  private final PresentationArtistMapper presentationArtistMapper;

  public DetailPresenter(String artistName, InteractorInvoker interactorInvoker,
      GetArtistInteractor getArtistInteractor,
      PresentationArtistMapper presentationArtistMapper, GroopifyTestViewInjector viewInjector) {
    super(viewInjector);
    this.artistName = artistName;
    this.interactorInvoker = interactorInvoker;
    this.getArtistInteractor = getArtistInteractor;
    this.presentationArtistMapper = presentationArtistMapper;
  }

  @Override public void onViewAttached() {
    getView().initUi();
  }

  public void onResume() {
    obtainArtist();
  }

  public void obtainArtist() {
    getArtistInteractor.setData(artistName);
    new InteractorExecution<>(getArtistInteractor).result(new InteractorResult<Artist>() {
      @Override public void onResult(Artist result) {
        getView().showArtistAlbums(presentationArtistMapper.map(result));
      }
    }).error(GetArtistsError.class, new InteractorResult<InteractorError>() {
      @Override public void onResult(InteractorError result) {
        getView().showGetArtistError();
      }
    }).execute(interactorInvoker);
  }
}
