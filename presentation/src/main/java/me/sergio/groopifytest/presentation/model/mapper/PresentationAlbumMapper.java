package me.sergio.groopifytest.presentation.model.mapper;

import me.sergio.groopifytest.domain.mappers.Mapper;
import me.sergio.groopifytest.domain.model.Album;
import me.sergio.groopifytest.presentation.model.AlbumPresentationBase;

/**
 * Created by sergio on 14/1/16.
 */
public class PresentationAlbumMapper implements Mapper<Album, AlbumPresentationBase> {
  @Override
  public AlbumPresentationBase map(Album model) {
    AlbumPresentationBase album = new AlbumPresentationBase();
    album.setName(model.getTitle());
    album.setPicture(model.getPicture());
    return album;
  }
}
