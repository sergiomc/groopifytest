package me.sergio.groopifytest.presentation.model.mapper;

import java.util.ArrayList;
import java.util.List;

import me.sergio.groopifytest.domain.mappers.Mapper;
import me.sergio.groopifytest.domain.model.Album;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.presentation.model.AlbumPresentationBase;
import me.sergio.groopifytest.presentation.model.ArtistPresentationBase;

public class PresentationArtistMapper implements Mapper<Artist, ArtistPresentationBase> {

  private static final PresentationAlbumMapper PRESENTATION_ALBUM_MAPPER = new PresentationAlbumMapper();

  @Override public ArtistPresentationBase map(Artist model) {
    if (model == null) {
      return null;
    }
    ArtistPresentationBase presentationArtist = new ArtistPresentationBase();
    presentationArtist.setName(model.getName());
    presentationArtist.setPicture(model.getPicture());
    List<AlbumPresentationBase> presentationAlbums = new ArrayList<>();
    if(model.getAlbums()!=null){
      for (Album album: model.getAlbums()) {
        presentationAlbums.add(PRESENTATION_ALBUM_MAPPER.map(album));
      }
    }
    presentationArtist.setAlbums(presentationAlbums);
    return presentationArtist;
  }
}