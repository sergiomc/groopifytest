package me.sergio.groopifytest.presentation;

public interface InteractorResult<T> {
  void onResult(T result);
}
