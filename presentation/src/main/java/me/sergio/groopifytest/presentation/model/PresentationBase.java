package me.sergio.groopifytest.presentation.model;

/**
 * Created by sergio on 15/1/16.
 */
public class PresentationBase {

  public String name;
  public String picture;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }
}
