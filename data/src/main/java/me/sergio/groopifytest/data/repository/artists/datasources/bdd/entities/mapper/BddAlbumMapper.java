package me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.mapper;

import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddAlbum;
import me.sergio.groopifytest.domain.mappers.TwoWaysMapper;
import me.sergio.groopifytest.domain.model.Album;

public class BddAlbumMapper implements TwoWaysMapper<BddAlbum, Album> {

  @Override public BddAlbum inverseMap(Album model) {
    if (model == null) {
      return null;
    }
    BddAlbum album = new BddAlbum();
    album.setTitle(model.getTitle());
    album.setPicture(model.getPicture());
    album.setReleaseDate(model.getReleaseDate());
    return album;
  }

  @Override public Album map(BddAlbum model) {
    if (model == null) {
      return null;
    }
    Album album = new Album();
    album.setTitle(model.getTitle());
    album.setPicture(model.getPicture());
    album.setReleaseDate(model.getReleaseDate());
    return album;
  }
}
