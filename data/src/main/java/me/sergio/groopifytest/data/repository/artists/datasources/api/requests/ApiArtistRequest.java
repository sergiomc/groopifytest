package me.sergio.groopifytest.data.repository.artists.datasources.api.requests;

/**
 * Created by sergio on 14/1/16.
 */
public class ApiArtistRequest {
  public int artistId;
  public int limit;

  public ApiArtistRequest(int artistId, int limit) {
    this.artistId = artistId;
    this.limit = limit;
  }
}
