package me.sergio.groopifytest.data.repository.caching.strategy;

public interface CachingStrategy<T> {
  boolean isValid(T data);
}
