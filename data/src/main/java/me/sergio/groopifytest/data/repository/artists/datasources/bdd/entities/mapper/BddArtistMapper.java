package me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.mapper;

import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddAlbum;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddArtist;
import me.sergio.groopifytest.domain.mappers.TwoWaysMapper;
import me.sergio.groopifytest.domain.model.Album;
import me.sergio.groopifytest.domain.model.Artist;

public class BddArtistMapper implements TwoWaysMapper<BddArtist, Artist> {

  private static final BddAlbumMapper ALBUM_MAPPER = new BddAlbumMapper();

  @Override public BddArtist inverseMap(Artist model) {
    if (model == null) {
      return null;
    }
    BddArtist artist = new BddArtist();
    artist.setName(model.getName());
    artist.setPicture(model.getPicture());
    artist.setArtistId(model.getArtistId());
    return artist;
  }

  @Override public Artist map(BddArtist model) {
    if (model == null) {
      return null;
    }
    Artist artist = new Artist();
    artist.setName(model.getName());
    artist.setPicture(model.getPicture());
    artist.setArtistId(model.getArtistId());
    for (BddAlbum bddAlbum: model.getBddAlbum()) {
      Album album = ALBUM_MAPPER.map(bddAlbum);
      artist.getAlbums().add(album);
    }
    return artist;
  }

}
