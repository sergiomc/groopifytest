package me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors;

import java.sql.SQLException;

import me.sergio.groopifytest.data.DatabaseHelper;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddArtist;

public class ArtistPersistor implements Persistor<BddArtist> {

  private DatabaseHelper helper;

  public ArtistPersistor(DatabaseHelper helper) {
    this.helper = helper;
  }

  @Override public void persist(BddArtist data) throws SQLException {
    if (data != null) {
      helper.getArtistDao().create(data);
    }
  }
}
