package me.sergio.groopifytest.data.repository.artists.datasources.api.entities;

import com.google.gson.annotations.Expose;

public class ApiResult {

  private final String ARTIST = "artist";

  @Expose public String wrapperType;
  @Expose public String artistType;
  @Expose public String artistName;
  @Expose public String artistLinkUrl;
  @Expose public String artistId;
  @Expose public String amgArtistId;
  @Expose public String primaryGenreName;
  @Expose public String primaryGenreId;
  @Expose public String ratioStationUrl;
  @Expose public String collectionType;
  @Expose public String collectionId;
  @Expose public String collectionName;
  @Expose public String collectionCensoredName;
  @Expose public String artistViewUrl;
  @Expose public String collectionViewUrl;
  @Expose public String artworkUrl60;
  @Expose public String artworkUrl100;
  @Expose public String collectionPrice;
  @Expose public String collectionExplicitness;
  @Expose public String trackCount;
  @Expose public String copyright;
  @Expose public String country;
  @Expose public String currency;
  @Expose public String releaseDate;

  public String getWrapperType() {
    return wrapperType;
  }

  public void setWrapperType(String wrapperType) {
    this.wrapperType = wrapperType;
  }

  public String getArtistType() {
    return artistType;
  }

  public void setArtistType(String artistType) {
    this.artistType = artistType;
  }

  public String getArtistName() {
    return artistName;
  }

  public void setArtistName(String artistName) {
    this.artistName = artistName;
  }

  public String getArtistLinkUrl() {
    return artistLinkUrl;
  }

  public void setArtistLinkUrl(String artistLinkUrl) {
    this.artistLinkUrl = artistLinkUrl;
  }

  public String getArtistId() {
    return artistId;
  }

  public void setArtistId(String artistId) {
    this.artistId = artistId;
  }

  public String getAmgArtistId() {
    return amgArtistId;
  }

  public void setAmgArtistId(String amgArtistId) {
    this.amgArtistId = amgArtistId;
  }

  public String getPrimaryGenreName() {
    return primaryGenreName;
  }

  public void setPrimaryGenreName(String primaryGenreName) {
    this.primaryGenreName = primaryGenreName;
  }

  public String getPrimaryGenreId() {
    return primaryGenreId;
  }

  public void setPrimaryGenreId(String primaryGenreId) {
    this.primaryGenreId = primaryGenreId;
  }

  public String getRatioStationUrl() {
    return ratioStationUrl;
  }

  public void setRatioStationUrl(String ratioStationUrl) {
    this.ratioStationUrl = ratioStationUrl;
  }


  public String getCollectionType() {
    return collectionType;
  }

  public void setCollectionType(String collectionType) {
    this.collectionType = collectionType;
  }

  public String getCollectionId() {
    return collectionId;
  }

  public void setCollectionId(String collectionId) {
    this.collectionId = collectionId;
  }

  public String getCollectionName() {
    return collectionName;
  }

  public void setCollectionName(String collectionName) {
    this.collectionName = collectionName;
  }

  public String getCollectionCensoredName() {
    return collectionCensoredName;
  }

  public void setCollectionCensoredName(String collectionCensoredName) {
    this.collectionCensoredName = collectionCensoredName;
  }

  public String getArtistViewUrl() {
    return artistViewUrl;
  }

  public void setArtistViewUrl(String artistViewUrl) {
    this.artistViewUrl = artistViewUrl;
  }

  public String getCollectionViewUrl() {
    return collectionViewUrl;
  }

  public void setCollectionViewUrl(String collectionViewUrl) {
    this.collectionViewUrl = collectionViewUrl;
  }

  public String getArtworkUrl60() {
    return artworkUrl60;
  }

  public void setArtworkUrl60(String artworkUrl60) {
    this.artworkUrl60 = artworkUrl60;
  }

  public String getArtworkUrl100() {
    return artworkUrl100;
  }

  public void setArtworkUrl100(String artworkUrl100) {
    this.artworkUrl100 = artworkUrl100;
  }

  public String getCollectionPrice() {
    return collectionPrice;
  }

  public void setCollectionPrice(String collectionPrice) {
    this.collectionPrice = collectionPrice;
  }

  public String getCollectionExplicitness() {
    return collectionExplicitness;
  }

  public void setCollectionExplicitness(String collectionExplicitness) {
    this.collectionExplicitness = collectionExplicitness;
  }

  public String getTrackCount() {
    return trackCount;
  }

  public void setTrackCount(String trackCount) {
    this.trackCount = trackCount;
  }

  public String getCopyright() {
    return copyright;
  }

  public void setCopyright(String copyright) {
    this.copyright = copyright;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public String getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(String releaseDate) {
    this.releaseDate = releaseDate;
  }

  public boolean isArtist(){
    if(wrapperType.equals(ARTIST)){
      return true;
    }
    return false;
  }
}
