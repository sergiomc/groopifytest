package me.sergio.groopifytest.data.repository.artists.datasources.bdd;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddAlbum;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddArtist;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors.Persistor;
import me.sergio.groopifytest.data.repository.caching.strategy.CachingStrategy;
import me.sergio.groopifytest.data.repository.caching.strategy.list.ListCachingStrategy;
import me.sergio.groopifytest.domain.mappers.TwoWaysMapper;
import me.sergio.groopifytest.domain.model.*;

public class ArtistLocalGatewayImp implements ArtistLocalGateway {

  private final Dao<BddArtist, Integer> daoArtists;
  private final Dao<BddAlbum, Integer> daoAlbums;
  private final Persistor<BddArtist> artistPersistor;
  private final Persistor<BddAlbum> albumPersistor;
  private final CachingStrategy<BddArtist> cachingStrategy;
  private final ListCachingStrategy<BddArtist> listCachingStrategy;
  private final TwoWaysMapper<BddArtist, Artist> artistMapper;
  private final TwoWaysMapper<BddAlbum, Album> albumMapper;

  public ArtistLocalGatewayImp(Persistor<BddArtist> artistPersistor, Persistor<BddAlbum> albumPersistor,
                               Dao<BddArtist, Integer> daoArtists, Dao<BddAlbum, Integer> daoAlbums,
                               CachingStrategy<BddArtist> cachingStrategy,
                               ListCachingStrategy<BddArtist> listCachingStrategy,
                               TwoWaysMapper<BddArtist, Artist> artistMapper,
                               TwoWaysMapper<BddAlbum, Album> albumMapper) {
    this.daoArtists = daoArtists;
    this.daoAlbums = daoAlbums;
    this.artistPersistor = artistPersistor;
    this.albumPersistor = albumPersistor;
    this.cachingStrategy = cachingStrategy;
    this.listCachingStrategy = listCachingStrategy;
    this.artistMapper = artistMapper;
    this.albumMapper = albumMapper;
  }

  @Override public List<Artist> obtainArtists() {
    try {
      List<BddArtist> bddArtists = daoArtists.queryForAll();
      if (!listCachingStrategy.isValid(bddArtists)) {
        deleteBddArtists(listCachingStrategy.candidatesToPurgue(bddArtists));
      }
      ArrayList<Artist> artists = new ArrayList<>();
      for (BddArtist bddArtist : bddArtists) {
        artists.add(artistMapper.map(bddArtist));
      }
      return artists;
    } catch (java.sql.SQLException e) {
      throw new LocalException();
    }
  }

  @Override public void persist(List<Artist> artists) {
    try {
      for (Artist artist : artists) {
        BddArtist bddArtist = artistMapper.inverseMap(artist);
        bddArtist.setPersistedTime(System.currentTimeMillis());
        artistPersistor.persist(bddArtist);
        persistAlbums(artist);
      }
    } catch (SQLException e) {
      throw new LocalException();
    }
  }

  private void persistAlbums(Artist artist) {
    try {
      for (Album album : artist.getAlbums()) {
        BddAlbum bddAlbum = albumMapper.inverseMap(album);
        bddAlbum.setArtistId(artist.getArtistId());
        albumPersistor.persist(bddAlbum);
      }
    } catch (SQLException e) {
      throw new LocalException();
    }
  }

  @Override public Artist obtain(String name) {
    try {
      BddArtist bddArtist = daoArtists.queryBuilder().where().eq(BddArtist.FIELD_NAME, name).queryForFirst();
      if (!cachingStrategy.isValid(bddArtist)) {
        return null;
      }
      if(bddArtist.getBddAlbum().isEmpty()){
        addAlbumsToArtist(bddArtist);
      }
      return artistMapper.map(bddArtist);
    } catch (Throwable e) {
      throw new LocalException();
    }
  }

  private void addAlbumsToArtist(BddArtist bddArtist) throws SQLException {
    List<BddAlbum> bddAlbums=daoAlbums.queryBuilder().where().eq(BddAlbum.ARTIST_FIELD, bddArtist.getArtistId()).query();
    bddArtist.getBddAlbum().addAll(bddAlbums);
  }

  @Override public void delete(List<Artist> purgue) {
    if (purgue != null && purgue.size() > 0) {
      try {
        List<String> deleteNames = new ArrayList<>();
        for (Artist purgueArtist : purgue) {
         deleteNames.add(purgueArtist.getName());
        }
        internalDeleteArtists(deleteNames);
      } catch (Throwable e) {
        throw new LocalException();
      }
    }
  }

  private void deleteBddArtists(List<BddArtist> purgue) throws SQLException {
    if (purgue != null && purgue.size() > 0) {
      List<String> deleteMd5s = new ArrayList<>();
      for (BddArtist purgueArtist : purgue) {
        deleteMd5s.add(purgueArtist.getName());
      }
      internalDeleteArtists(deleteMd5s);
    }
  }

  private void internalDeleteArtists(List<String> deleteNames) throws SQLException {
    DeleteBuilder<BddArtist, Integer> deleteBuilder = daoArtists.deleteBuilder();
    deleteBuilder.where().in(BddArtist.FIELD_NAME, deleteNames);
    deleteBuilder.delete();
  }
}
