package me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities;

import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;
import java.util.List;

import me.sergio.groopifytest.data.repository.caching.strategy.ttl.TtlCachingObject;

public class BddArtist implements TtlCachingObject {

  public static final String FIELD_ID = "id";
  public static final String FIELD_NAME = "name";

  @DatabaseField(generatedId = true, columnName = FIELD_ID) private int id;
  @DatabaseField public int artistId;
  @DatabaseField public String name;
  @DatabaseField public String picture;
  @DatabaseField public long persistedTime;
  public List<BddAlbum> bddAlbum = new ArrayList<>();

  public int getArtistId() {
    return artistId;
  }

  public void setArtistId(int artistId) {
    this.artistId = artistId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public List<BddAlbum> getBddAlbum() {
    return bddAlbum;
  }

  public void setBddAlbum(List<BddAlbum> bddAlbum) {
    bddAlbum = bddAlbum;
  }

  @Override public long getPersistedTime() {
    return persistedTime;
  }

  public void setPersistedTime(long persistedTime) {
    this.persistedTime = persistedTime;
  }
}
