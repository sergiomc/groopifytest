package me.sergio.groopifytest.data.repository.artists.datasources.api.entities.mapper;

import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.ApiResult;
import me.sergio.groopifytest.domain.mappers.Mapper;
import me.sergio.groopifytest.domain.model.Artist;

public class ApiArtistMapper implements Mapper<ApiResult, Artist> {

  @Override public Artist map(ApiResult model) {
    if (model == null) {
      return null;
    }
    Artist artist = new Artist();
    artist.setName(model.getArtistName());
    artist.setArtistId(Integer.parseInt(model.getArtistId()));


    return artist;
  }
}
