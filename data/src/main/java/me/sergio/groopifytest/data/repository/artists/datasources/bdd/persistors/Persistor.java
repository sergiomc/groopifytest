package me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors;

import java.sql.SQLException;

public interface Persistor<T> {
  void persist(T data) throws SQLException;
}
