package me.sergio.groopifytest.data.repository.artists.datasources.api.responses;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.ApiResult;

public class ApiResponse {

  @Expose private List<ApiResult> results = new ArrayList<>();

  public List<ApiResult> getResults() {
    return results;
  }

  public void setResults(List<ApiResult> results) {
    this.results = results;
  }
}