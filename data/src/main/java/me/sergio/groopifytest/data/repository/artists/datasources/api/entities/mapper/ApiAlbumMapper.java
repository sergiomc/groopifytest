package me.sergio.groopifytest.data.repository.artists.datasources.api.entities.mapper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.ApiResult;
import me.sergio.groopifytest.domain.mappers.Mapper;
import me.sergio.groopifytest.domain.model.Album;

public class ApiAlbumMapper implements Mapper<ApiResult, Album> {

  @Override public Album map(ApiResult model) {
    if (model == null) {
      return null;
    }

    Album album = new Album();
    album.setTitle(model.getCollectionName());
    album.setPicture(model.getArtworkUrl100());
    album.setReleaseDate(stringToDate(model.getReleaseDate()));
    return album;
  }

  private Date stringToDate(String dateString) {
    Date date = new Date();
    DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
    try {
       date = format.parse(dateString);
    } catch (ParseException e) {
      e.printStackTrace();
    }
    return date;
  }
}
