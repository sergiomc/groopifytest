package me.sergio.groopifytest.data.repository.artists.datasources.bdd.persistors;

import java.sql.SQLException;

import me.sergio.groopifytest.data.DatabaseHelper;
import me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities.BddAlbum;

public class AlbumPersistor implements Persistor<BddAlbum> {

  private DatabaseHelper helper;

  public AlbumPersistor(DatabaseHelper helper) {
    this.helper = helper;
  }

  @Override public void persist(BddAlbum data) throws SQLException {
    if (data != null) {
      helper.getAlbumDao().create(data);
    }
  }
}
