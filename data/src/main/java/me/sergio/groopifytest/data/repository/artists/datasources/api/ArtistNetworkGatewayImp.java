package me.sergio.groopifytest.data.repository.artists.datasources.api;

import java.util.ArrayList;
import java.util.List;

import me.sergio.groopifytest.data.repository.artists.datasources.api.entities.ApiResult;
import me.sergio.groopifytest.data.repository.artists.datasources.api.requests.ApiArtistRequest;
import me.sergio.groopifytest.data.repository.artists.datasources.api.responses.ApiResponse;
import me.sergio.groopifytest.domain.mappers.Mapper;
import me.sergio.groopifytest.domain.model.Album;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.model.ArtistNetworkGateway;
import me.sergio.groopifytest.domain.model.NetworkException;

public class ArtistNetworkGatewayImp implements ArtistNetworkGateway {

  public static final String ALBUM = "album";
  public static final String MUSIC = "music";
  private final ArtistApiService apiService;
  private final Mapper<ApiResult, Artist> artistMapper;
  private final Mapper<ApiResult, Album> albumMapper;

  public ArtistNetworkGatewayImp(ArtistApiService apiService,
                                 Mapper<ApiResult, Artist> artistMapper,
                                 Mapper<ApiResult, Album> albumMapper) {
    this.apiService = apiService;
    this.artistMapper = artistMapper;
    this.albumMapper = albumMapper;
  }

  @Override public List<Artist> obtainArtists() throws NetworkException {
    try {

      List<Artist> artists = new ArrayList<>();
      ApiArtistRequest artistRequest = new ApiArtistRequest(909253, 10);
      artists.add(obtainArtist(artistRequest));
      artistRequest = new ApiArtistRequest(6906197, 20);
      artists.add(obtainArtist(artistRequest));
      artistRequest = new ApiArtistRequest(16252655, 7);
      artists.add(obtainArtist(artistRequest));
      return artists;

    } catch (Throwable e) {
      throw new NetworkException();
    }
  }

  private Artist obtainArtist(ApiArtistRequest request) throws java.io.IOException {
    Artist artist = new Artist();
    List<Album> albums = new ArrayList<>();
    ApiResponse apiResponse = apiService.obtainArtist(request.artistId, ALBUM, MUSIC, request.limit).execute().body();
    List<ApiResult> results = apiResponse.getResults();

    if (results != null) {
      for (ApiResult result : results) {
        if(result.isArtist()){
          artist = artistMapper.map(result);
        }else{
          albums.add(albumMapper.map(result));
        }
      }
      artist.setAlbums(albums);
      artist.setPicture(getPictureLastAlbum(albums));
    }
    return artist;
  }

  private String getPictureLastAlbum(List<Album> albums) {
    Album lastReleaseAlbum = new Album();
    for (Album album: albums) {
      if(lastReleaseAlbum.getReleaseDate().before(album.getReleaseDate())){
        lastReleaseAlbum=album;
      }
    }
    return lastReleaseAlbum.getPicture();
  }


}
