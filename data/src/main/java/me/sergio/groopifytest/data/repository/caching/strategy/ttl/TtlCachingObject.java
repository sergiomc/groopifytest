package me.sergio.groopifytest.data.repository.caching.strategy.ttl;

public interface TtlCachingObject {
  long getPersistedTime();
}
