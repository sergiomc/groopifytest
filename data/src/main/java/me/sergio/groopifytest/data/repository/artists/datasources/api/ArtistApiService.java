package me.sergio.groopifytest.data.repository.artists.datasources.api;

import me.sergio.groopifytest.data.repository.artists.datasources.api.responses.ApiResponse;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

public interface ArtistApiService {

  @GET("lookup")
  Call<ApiResponse> obtainArtist(@Query("id") int id,@Query("entity") String entity, @Query("media") String media, @Query("limit") int limit);
}
