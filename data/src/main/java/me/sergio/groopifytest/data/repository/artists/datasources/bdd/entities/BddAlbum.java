package me.sergio.groopifytest.data.repository.artists.datasources.bdd.entities;

import com.j256.ormlite.field.DatabaseField;

import java.util.Date;

public class BddAlbum {

  public static final String ARTIST_FIELD= "artist_id";

  @DatabaseField(generatedId = true, columnName = "id") private int id;
  @DatabaseField(columnName = ARTIST_FIELD ) private int artistId;
  @DatabaseField public String title;
  @DatabaseField public String picture;
  @DatabaseField public Date releaseDate;

  public int getArtistId() {
    return artistId;
  }

  public void setArtistId(int artistId) {
    this.artistId = artistId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Date getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(Date releaseDate) {
    this.releaseDate = releaseDate;
  }
}
