package me.sergio.groopifytest.domain.interactors.artists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import me.sergio.groopifytest.domain.interactors.InteractorResponse;
import me.sergio.groopifytest.domain.model.*;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by sergio on 15/1/16.
 */
@RunWith(MockitoJUnitRunner.class) public class GetArtistsInteractorShould {

  private static final List<Artist> EMPTY_LIST = Collections.emptyList();
  public static final List<Artist> ARTISTS = Collections.singletonList(new Artist());

  @Mock
  ArtistLocalGateway localGateway;
  @Mock
  ArtistNetworkGateway networkGateway;

  private GetArtistsInteractor interactor;

  @Before
  public void initialise() {
    interactor = new GetArtistsInteractor(localGateway, networkGateway);
  }

  @Test public void return_artists_and_update_local_cache_when_network_returns_data(){
    when(networkGateway.obtainArtists()).thenReturn(ARTISTS);

    InteractorResponse<List<Artist>> response = interactor.call();

    verify(localGateway).persist(ARTISTS);
    assertThatResponseHasResult(response);

  }

  @Test public void return_local_artists_when_local_storage_is_populated() {
    when(localGateway.obtainArtists()).thenReturn(ARTISTS);

    InteractorResponse<List<Artist>> response = interactor.call();

    assertThatResponseHasResult(response);
  }

  @Test
  public void return_an_error_when_network_fails() {
    when(localGateway.obtainArtists()).thenReturn(EMPTY_LIST);
    when(networkGateway.obtainArtists()).thenThrow(new NetworkException());

    InteractorResponse<List<Artist>> result = interactor.call();

    assertThatResponseHasError(result);
  }

  @Test public void hit_network_when_local_fails() {
    when(localGateway.obtainArtists()).thenThrow(new LocalException());

    interactor.call();

    verify(networkGateway).obtainArtists();
  }

  @Test public void hit_network_when_local_is_empty() {
    when(localGateway.obtainArtists()).thenReturn(EMPTY_LIST);

    interactor.call();

    verify(networkGateway).obtainArtists();
  }



  private void assertThatResponseHasResult(InteractorResponse<List<Artist>> response) {
    assertThat(response.getError(), is(nullValue()));
    assertThat(response.getResult(), is(ARTISTS));
  }

  private void assertThatResponseHasError(InteractorResponse<List<Artist>> result) {
    assertThat(result.getError(), is(notNullValue()));
    assertThat(result.getResult(), is(nullValue()));
  }
}
