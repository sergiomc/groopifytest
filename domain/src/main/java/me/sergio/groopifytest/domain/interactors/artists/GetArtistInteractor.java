package me.sergio.groopifytest.domain.interactors.artists;

import me.sergio.groopifytest.domain.interactors.Interactor;
import me.sergio.groopifytest.domain.interactors.InteractorResponse;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.model.ArtistLocalGateway;
import me.sergio.groopifytest.domain.model.LocalException;

public class GetArtistInteractor implements Interactor<InteractorResponse<Artist>> {

  private ArtistLocalGateway localGateway;
  private String artistName;

  public GetArtistInteractor(ArtistLocalGateway localGateway) {
    this.localGateway = localGateway;
  }

  public void setData(String artistName) {
    this.artistName = artistName;
  }

  @Override public InteractorResponse<Artist> call() {
    try {
      return new InteractorResponse<>(localGateway.obtain(artistName));
    } catch (LocalException e) {
      return new InteractorResponse<>(new GetArtistsError());
    }
  }
}
