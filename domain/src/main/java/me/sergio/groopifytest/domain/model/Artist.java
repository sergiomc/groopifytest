package me.sergio.groopifytest.domain.model;

import java.util.ArrayList;
import java.util.List;

public class Artist {

  int artistId;
  String name;
  String picture;
  List<Album> albums = new ArrayList<>();


  public int getArtistId() {
    return artistId;
  }

  public void setArtistId(int artistId) {
    this.artistId = artistId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public List<Album> getAlbums() {
    return albums;
  }

  public void setAlbums(List<Album> albums) {
    this.albums = albums;
  }
}
