package me.sergio.groopifytest.domain.model;

import java.util.Date;

public class Album {

  String title;
  String picture;
  Date releaseDate;

  public Album() {
    releaseDate = new Date(0);
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPicture() {
    return picture;
  }

  public void setPicture(String picture) {
    this.picture = picture;
  }

  public Date getReleaseDate() {
    return releaseDate;
  }

  public void setReleaseDate(Date releaseDate) {
    this.releaseDate = releaseDate;
  }
}
