package me.sergio.groopifytest.domain.model;

import java.util.List;

public interface ArtistLocalGateway extends ArtistProvider {

  void persist(List<Artist> artists);

  Artist obtain(String name);

  void delete(List<Artist> deleted);
}
