package me.sergio.groopifytest.domain.interactors.artists;

import java.util.List;
import me.sergio.groopifytest.domain.interactors.Interactor;
import me.sergio.groopifytest.domain.interactors.InteractorResponse;
import me.sergio.groopifytest.domain.model.Artist;
import me.sergio.groopifytest.domain.model.ArtistLocalGateway;
import me.sergio.groopifytest.domain.model.ArtistNetworkGateway;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public class GetArtistsInteractor implements Interactor<InteractorResponse<List<Artist>>> {

  private final ArtistLocalGateway localGateway;
  private final ArtistNetworkGateway networkGateway;

  public GetArtistsInteractor(ArtistLocalGateway localGateway,
                              ArtistNetworkGateway networkGateway) {
    this.localGateway = localGateway;
    this.networkGateway = networkGateway;
  }

  @Override public InteractorResponse<List<Artist>> call() {
    return new Chain.Builder<List<Artist>>().typeChecker(artistListTypeChecker())
        .providers(asList(localGateway(), networkGateway()))
        .storers(singletonList(localGatewayStore()))
        .build()
        .obtain();
  }

  private ListTypeChecker<Artist> artistListTypeChecker() {
    return new ListTypeChecker<>(new GetArtistsError());
  }

  private Chain.TypeStorer<List<Artist>> localGatewayStore() {
    return new Chain.TypeStorer<List<Artist>>() {
      @Override public void store(List<Artist> artists) {
        localGateway.persist(artists);
      }
    };
  }

  private Chain.TypeProvider<List<Artist>> networkGateway() {
    return new Chain.TypeProvider<List<Artist>>() {
      @Override public List<Artist> obtain() {
        return networkGateway.obtainArtists();
      }
    };
  }

  private Chain.TypeProvider<List<Artist>> localGateway() {
    return new Chain.TypeProvider<List<Artist>>() {
      @Override public List<Artist> obtain() {
        return localGateway.obtainArtists();
      }
    };
  }
}
