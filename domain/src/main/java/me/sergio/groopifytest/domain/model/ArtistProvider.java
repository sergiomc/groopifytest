package me.sergio.groopifytest.domain.model;

import java.util.List;

public interface ArtistProvider {
  List<Artist> obtainArtists();
}
