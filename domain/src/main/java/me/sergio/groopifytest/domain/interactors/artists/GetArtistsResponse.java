package me.sergio.groopifytest.domain.interactors.artists;

import me.sergio.groopifytest.domain.interactors.InteractorError;
import me.sergio.groopifytest.domain.interactors.InteractorResponse;
import me.sergio.groopifytest.domain.model.Artist;

public class GetArtistsResponse extends InteractorResponse<Artist> {
  public GetArtistsResponse(InteractorError error) {
    super(error);
  }

  public GetArtistsResponse(Artist result) {
    super(result);
  }
}
