package me.sergio.groopifytest.domain.interactors.artists;

import java.util.Collections;
import java.util.List;
import me.sergio.groopifytest.domain.interactors.InteractorError;

class ListTypeChecker<T> implements Chain.TypeChecker<List<T>> {

  private final InteractorError error;

  ListTypeChecker(InteractorError error) {
    this.error = error;
  }

  @Override public boolean isValid(List<T> object) {
    return !object.isEmpty();
  }

  @Override public List<T> empty() {
    return Collections.emptyList();
  }

  @Override public InteractorError error() {
    return error;
  }
}
